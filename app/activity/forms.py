# -*- coding: utf-8 -*-

from django import forms
from core.models import Activity, Photo, Video, Document, PhotoCategory, VideoCategory, DocumentCategory
from crispy_forms.layout import LayoutObject, TEMPLATE_PACK
from django.template.loader import render_to_string

# -----------------------------------------------------------------------------
# Project
# -----------------------------------------------------------------------------

class Formset(LayoutObject):

    template = "frontend/formset.html"

    def __init__(self, formset_name_in_context, template=None):
        self.formset_name_in_context = formset_name_in_context
        self.fields = []
        if template:
            self.template = template

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        formset = context[self.formset_name_in_context]
        return render_to_string(self.template, {'formset': formset})


class ActivityForm(forms.ModelForm):
 
    class Meta():
        model = Activity
        fields = ['title','status','discription','qr_code','project','tag_name']
        
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)

        super(ActivityForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class' : 'form-control'})

        self.fields['title'].widget.attrs['placeholder'] = 'Enter activity name'   
        self.fields['tag_name'].widget.attrs['placeholder'] = 'Enter a QRcode tagname'   
        self.fields['discription'].widget.attrs['placeholder'] = 'Enter discription'   
        self.fields['discription'].widget.attrs.update({'class' : 'form-control','cols':'5','rows':'3'})
        self.fields['discription'].required = False
        self.fields['project'].required = False
        self.fields['title'].required = True
        self.fields['discription'].required = True

    def save(self, commit=True):
        instance = super().save(commit=False)

        if commit:
            activity=instance.save()

        return instance


class PhotoForm(forms.ModelForm):
    class Meta():
        model = Photo
        fields = ['url','activity']


class VideoForm(forms.ModelForm):
    class Meta():
        model = Video
        fields = ['url','activity']


class DocumentForm(forms.ModelForm):
    class Meta():
        model = Document
        fields = ['url','activity']


class ActivityEditForm(forms.ModelForm):

    class Meta():
        model = Activity
        fields = ["id",'title','status','discription','qr_code','tag_name']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)

        super(ActivityEditForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class' : 'form-control'})

        self.fields['title'].widget.attrs['placeholder'] = 'Enter activity name'   
        self.fields['tag_name'].widget.attrs['placeholder'] = 'Enter QRcode tagname'   
        self.fields['discription'].widget.attrs['placeholder'] = 'Enter discription'   
        self.fields['discription'].widget.attrs.update({'class' : 'form-control','cols':'5','rows':'3'})
        self.fields['discription'].required = False

       

class PhotoCategoryForm(forms.ModelForm):
    class Meta():
        model = PhotoCategory
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(PhotoCategoryForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['placeholder'] = 'Enter name'  


class VideoCategoryForm(forms.ModelForm):
    class Meta():
        model = VideoCategory
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(VideoCategoryForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['placeholder'] = 'Enter name'  
    

class DocumentCategoryForm(forms.ModelForm):
    class Meta():
        model = DocumentCategory
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(DocumentCategoryForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['name'].widget.attrs['placeholder'] = 'Enter name'  
    