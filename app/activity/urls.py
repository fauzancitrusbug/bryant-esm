# -*- coding: utf-8 -*-

from django.urls import path, include
from django.views.generic import TemplateView
from .views import (ActivityCreateView,
    ActivityView,
    ActivityDeleteView,
    ActivityUpdateView,
    ActivityUpdatesView, 
    Ajax_delete_photos,
    Ajax_delete_video,
    Ajax_delete_doc,
    Ajax_upload_photos,
    add_photo_category,
    PhotoCategoryEditView,
    PhotoCategoryDeleteView,
    add_video_category,
    VideoCategoryEditView,
    VideoCategoryDeleteView,
    Ajax_upload_document,
    add_document_category,
    DocumentCategoryDeleteView,
    DocumentCategoryFormEditView,
)

urlpatterns = [

    path("activity-list",ActivityView.as_view(), name="activity-list"),
    path("activity-create",ActivityCreateView.as_view(), name="activity-create"),
    path("activity-edit/<int:pk>",ActivityUpdateView.as_view(), name="activity-edit"),
    path("activity-edit-post/",ActivityUpdatesView.as_view(), name="activity-edit-post"),
    path("activity-delete",ActivityDeleteView, name="activity-delete"),
    path("delete-photos",Ajax_delete_photos, name="delete-photos"),
    path("delete-video",Ajax_delete_video, name="delete-video"),
    path("delete-doc",Ajax_delete_doc, name="delete-doc"),
    path("upload-photos",Ajax_upload_photos, name="upload-photos"),

    # Photo Category
    path("add-photo-category",add_photo_category, name="category"),
    path("edit-photo-category",PhotoCategoryEditView.as_view(), name="edit-photo-category"),
    path("delete-photo-category",PhotoCategoryDeleteView, name="delete-photo-category"),

    # video Category
    path("add-video-category",add_video_category, name="video-catgeory"),
    path("edit-video-category",VideoCategoryEditView.as_view(), name="edit-video-category"),
    path("delete-video-category",VideoCategoryDeleteView, name="delete-video-category"),

    # Doc category
    path("upload-document",Ajax_upload_document, name="upload-document"),    
    path("add-doc-category",add_document_category, name="doc-catgeory"),
    path("edit-doc-category",DocumentCategoryFormEditView.as_view(), name="edit-doc-category"),
    path("delete-doc-category",DocumentCategoryDeleteView, name="delete-doc-category"),

]




