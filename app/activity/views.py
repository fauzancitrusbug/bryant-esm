
from django.views.generic import TemplateView
from core.models import Activity, Photo, Video, Document, ActivityUser, Project, PhotoCategory, VideoCategory, DocumentCategory
from .forms import ActivityForm,ActivityEditForm, PhotoCategoryForm, VideoCategoryForm, DocumentCategoryForm
from http import HTTPStatus
from django.shortcuts import render, redirect,get_object_or_404, HttpResponse
from django.http import JsonResponse, HttpResponse

from app.activityuser.forms import ActivityUserForm

from pathlib import Path
from django.conf import settings
import pyqrcode
from django.contrib import messages
 
from core.utils.storages import MediaRootS3Boto3Storage
import boto3
import base64
import hashlib
import hmac
import os
import time

from rest_framework.response import Response
from rest_framework import  status
from rest_framework.decorators import api_view, renderer_classes, authentication_classes, permission_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
import datetime
import subprocess
from django.conf import settings

from bryant.settings import (
    AWS_STORAGE_BUCKET_NAME,
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY,
    AWS_S3_REGION_NAME
)

from core.utils.s3_media_file import (
    get_s3_store_directory_file,
    upload_photo_s3_url_to_file,
    get_filename_from_url
)



class ActivityCreateView(TemplateView):

    model = Activity
    template_name = "frontend/activity/add-new-activity.html"
    

    def get(self, request):
        project_obj = request.GET['id']

        acivity_obj = Activity.objects.all()
        user = ActivityUser.objects.all()

        form_class = ActivityForm
        activity_user = ActivityUserForm

        context = {
            "title": "Activity",
            "form":form_class,
            "instance":acivity_obj,
            "user":user,
            "activity_user":activity_user,
            "project_obj":project_obj,
        }
        
        return render(request, self.template_name,context)    

    def post(self, request):

        get_video = request.FILES.getlist('video')
        get_document = request.FILES.getlist('docuemnts')

        project_id = request.POST['project_id']
        project_object = Project.objects.get(id=project_id) 
        create_form = ActivityForm(request.POST)

        if create_form.is_valid():
            activity=create_form.save(request)
            url = f"{str(activity.id)}|{settings.QR_CODE_URL}"
            data = str(activity.id)
            qr = pyqrcode.create(url)

            name = 'qr_code/'+data+'.png'
            Path(settings.MEDIA_ROOT+"/qr_code/").mkdir(parents=True, exist_ok=True)
            
            qr.png(settings.MEDIA_ROOT+'/'+name , scale=8)
         
            # organize a path for the file in bucket
            file_path_within_bucket = 'media/qr_code/'+data+'.png'

            media_storage = MediaRootS3Boto3Storage()
            s3_client = boto3.client('s3')
            response = s3_client.upload_file(file_path_within_bucket, settings.AWS_STORAGE_BUCKET_NAME, file_path_within_bucket)

            activity.qr_code = name
            activity.project = project_object
            activity.save()
            if len(get_video)!=0:
                for ob in get_video:
                    k=Video()
                    k.name = ob.name
                    k.activity = activity
                    k.url=ob
                    k.save()

            if len(get_document)!=0:
                for ob in get_document:
                    k=Document()
                    k.name = ob.name
                    k.activity = activity
                    k.url=ob
                    k.save()

            messages.success(request, "Created {0} activity successfully".format(activity.title))
            # return redirect('/project/project-actvity/'+project_id)
            return redirect('/activity/activity-edit/'+str(activity.id))    
        else:
            messages.success(request, "Not created")
            return redirect('/project/project-actvity/'+project_id)


class ActivityView(TemplateView):
    
    template_name = "frontend/activity/all-activity.html"

    def get(self, request):
        acivity_obj = Activity.objects.all()
        form_class = ActivityForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            acivity_obj = Activity.objects.get(id=get_id)
            form = ActivityForm(instance=acivity_obj)
            return HttpResponse(form.as_p())
        else:
            context = {
                    "title": "Activity",
                    "form":form_class,
                    "instance":acivity_obj
            }
            return render(request, self.template_name,context)    
 
    
class ActivityUpdateView(TemplateView):
    
    template_name = "frontend/activity/edit-activity.html"

    def get(self, request,pk):
        acivity_obj = Activity.objects.all()
        form_class = ActivityEditForm

        acivity_obj = Activity.objects.get(id=pk)
        form = ActivityEditForm(instance=acivity_obj) 
        user = ActivityUser.objects.filter(activity=pk)

        activity_user = ActivityUserForm
        
        img_cat = PhotoCategory.objects.filter(activity=acivity_obj)
        categ=request.GET.get('categ')
        img_instance = Photo.objects.filter(activity=acivity_obj, photo_category__id=categ)

        video_category = VideoCategory.objects.filter(activity=acivity_obj)
        video_categ=request.GET.get('vide_categ')
        video_instance = Video.objects.filter(activity=acivity_obj, video_category__id=video_categ).filter(uploaded=True)
        
        doc_categ=request.GET.get('doc_categ')
        doc_instance = Document.objects.filter(activity=acivity_obj, document_category__id=doc_categ)
        document_category = DocumentCategory.objects.filter(activity=acivity_obj)

        photo_cat = PhotoCategory.objects.filter(activity__id=pk)
        s3_photo_object_list = get_s3_store_directory_file()
        s3_document_object_list = get_s3_store_directory_file(directory_path="media/document/")
        s3_photo_video_list = get_s3_store_directory_file(directory_path="media/video/")

        context = {
                "title": "Activity",
                "form":form,
                "instance":acivity_obj,
                "user":user,
                'img_cat':img_cat,
                "activity_user":activity_user,
                "img_instance":img_instance,
                "video_instance":video_instance,
                "doc_instance":doc_instance,
                "AWS_BUCKET_URL":settings.AWS_BUCKET_URL,
                "photo_cat":photo_cat,
                "video_category":video_category,
                "document_category":document_category,
                "s3_photo_object_list":s3_photo_object_list,
                "s3_document_object_list":s3_document_object_list
        }
        return render(request, self.template_name, context)
 

class ActivityUpdatesView(TemplateView):
    def post(self,request):
        get_id = request.POST.get('get_activty_id')
        if get_id:
            acivity_obj = get_object_or_404(Activity, pk=get_id)
            if acivity_obj:
                form = ActivityEditForm(request.POST, instance=acivity_obj)
                if form.is_valid():
                    activity = form.save()
                messages.success(request, "Updated {0} activity successfully".format(activity.title))
                return redirect('/project/project-actvity/'+str(acivity_obj.project.id))
        else:
            messages.success(request, "Bad request")
            return redirect('/activity/activity-edit/'+get_id)

        
def ActivityDeleteView(request):

    get_id = request.POST['get_id']
    if get_id:
        Activity.objects.get(id=get_id).delete()
        response = {
                'status': 'Deleted',
                'ok': True
            }
        messages.success(request, "Deleted activity successfully")
        return JsonResponse(response)
    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)      



def Ajax_delete_photos(request):
    
    get_id = request.POST['get_id']
    if get_id:
        Photo.objects.get(id=get_id).delete()
        response = {
                'status': 'Deleted',
                'ok': True
            }
        return JsonResponse(response)
    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)   


def Ajax_delete_video(request):
    
    get_id = request.POST['get_id']
    if get_id:
        Video.objects.get(id=get_id).delete()
        response = {
                'status': 'Deleted',
                'ok': True
            }
        return JsonResponse(response)
    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)   


def Ajax_delete_doc(request):
    
    get_id = request.POST['get_id']
    if get_id:
        Document.objects.get(id=get_id).delete()
        response = {
                'status': 'Deleted',
                'ok': True
            }
        return JsonResponse(response)
    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)


def Ajax_upload_photos(request):
    exist_file_url = request.POST['exist_file_url']
    get_id = request.POST['get_activity_id']
    get_cat_id = request.POST['get_category_id']
    name = request.POST['photo_title']
    photo_description = request.POST['photo_description']
    uplod_photo = request.FILES.get("get_photo","")
    if get_id and get_cat_id:
        actvity_obj = Activity.objects.get(id=get_id)
        category_obj = PhotoCategory.objects.get(id=get_cat_id)
        photo_obj = Photo()
        photo_obj.activity=actvity_obj
        photo_obj.photo_category=category_obj
        photo_obj.name=name
        photo_obj.discription=photo_description
        photo_obj.save()
        if exist_file_url:
            upload_photo_s3_url_to_file(exist_file_url, photo_obj)
        else:
            photo_obj.url=uplod_photo
            photo_obj.save()
        response = {
                'status': 'Updated',
                'ok': True
            }
        messages.success(request, "Uploaded Image sudcessfully")
        return redirect('/activity/activity-edit/'+get_id+"?categ="+get_cat_id)    
    else:
        messages.success(request, "Bad Request")
        return redirect('app:project-list')


@api_view(('POST',))
# @renderer_classes((JSONRenderer.))
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])

def Ajax_upload_videos(request):

    AWS_UPLOAD_BUCKET = AWS_STORAGE_BUCKET_NAME
    AWS_UPLOAD_REGION = AWS_S3_REGION_NAME
    AWS_UPLOAD_ACCESS_KEY_ID = AWS_ACCESS_KEY_ID
    AWS_UPLOAD_SECRET_KEY = AWS_SECRET_ACCESS_KEY

    """
    The initial post request includes the filename
    and auth credientails. In our case, we'll use
    Session Authentication but any auth should work.
    """
    # filename_req = request.data.get('filename')
    filename_req =request.POST['filename']
    if not filename_req:
            return Response({"message": "A filename is required"}, status=status.HTTP_400_BAD_REQUEST)
    policy_expires = int(time.time()+50000)
    # activity_id = request.activty_id
    activity_id =request.POST['activty_id']
    video_catgory_id =request.POST['video_catgory']
    category_obj = VideoCategory.objects.get(id=video_catgory_id)
    
    # print('*************************************************************')
    # print(activity_id)
    # print(request.POST)
    # print('*************************************************************')


    # username_str = str(request.user.username)
    """
    Below we create the Django object. We'll use this
    in our upload path to AWS. 

    Example:
    To-be-uploaded file's name: Some Random File.mp4
    Eventual Path on S3: <bucket>/username/2312/2312.mp4
    """
    file_obj = Video.objects.create(activity=Activity.objects.get(id=activity_id), name=filename_req, video_category=category_obj)
    file_obj_id = file_obj.id
    upload_start_path = "{activity_id}/{file_obj_id}/".format(
                activity_id = activity_id,
                file_obj_id=file_obj_id
        )       
    _, file_extension = os.path.splitext(filename_req)
    filename_final = "{file_obj_id}{file_extension}".format(
                file_obj_id= file_obj_id,
                file_extension=file_extension

            )
    """
    Eventual file_upload_path includes the renamed file to the 
    Django-stored FileItem instance ID. Renaming the file is 
    done to prevent issues with user generated formatted names.
    """
    final_upload_path = "{upload_start_path}{filename_final}".format(
                                upload_start_path=upload_start_path,
                                filename_final=filename_final,
                        )
    if filename_req and file_extension:
        """
        Save the eventual path to the Django-stored FileItem instance
        """
        file_obj.url = final_upload_path
        file_obj.save()

    today = datetime.date.today()
    tomorrow = today + datetime.timedelta(days = 1) 
    policy_document_context = {
        # "expire": policy_expires,
       
        # "expire": datetime.datetime.now().isoformat(timespec='seconds'),
        "expire": tomorrow.strftime("%Y-%m-%dT%H:%M:%S"),
        "bucket_name": AWS_UPLOAD_BUCKET,
        "key_name": "",
        "acl_name": "private",
        "content_name": "",
        "content_length": 524288000,
        "upload_start_path": upload_start_path,

        }
    #2019-01-01T00:00:00Z
    policy_document = """
    {"expiration": "%(expire)sZ",
        "conditions": [ 
        {"bucket": "%(bucket_name)s"}, 
        ["starts-with", "$key", "%(upload_start_path)s"],
        {"acl": "%(acl_name)s"},
        
        ["starts-with", "$Content-Type", "%(content_name)s"],
        ["starts-with", "$filename", ""],
        ["content-length-range", 0, %(content_length)d]
        ]
    }
    """ % policy_document_context
    aws_secret = str.encode(AWS_UPLOAD_SECRET_KEY)
    policy_document_str_encoded = str.encode(policy_document.replace(" ", ""))
    url = 'https://{bucket}.s3-{region}.amazonaws.com/'.format(
                    bucket=AWS_UPLOAD_BUCKET,  
                    region=AWS_UPLOAD_REGION
                    )
    policy = base64.b64encode(policy_document_str_encoded)
    signature = base64.b64encode(hmac.new(aws_secret, policy, hashlib.sha1).digest())
    data = {
        "policy": policy,
        "signature": signature,
        "key": AWS_UPLOAD_ACCESS_KEY_ID,
        "file_bucket_path": upload_start_path,
        "file_id": file_obj_id,
        "filename": filename_final,
        "url": url,
        "activity_id": activity_id,
    }
    return Response(data, status=status.HTTP_200_OK)



@api_view(('GET',))
# @renderer_classes((JSONRenderer.))
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])

def Ajax_update_videos(request):
    file_id = request.GET['file']  
    data = {}
     
    if file_id:
        obj = Video.objects.get(id=int(file_id))
        obj.uploaded = True

        thumb_outputFile = settings.MEDIA_ROOT+"\\video\\thumb\\thumb.jpeg"
        main_outputFile = settings.MEDIA_ROOT+"\\video\\main\\main.jpeg"

        Path(settings.MEDIA_ROOT+"/video/thumb/").mkdir(parents=True, exist_ok=True)
        Path(settings.MEDIA_ROOT+"/video/main/").mkdir(parents=True, exist_ok=True)


        thumb_file = 'video/thumb/{0}.jpeg'.format(obj.id)
        main_file = 'video/main/{0}.jpeg'.format(obj.id)

        input_path = 'https://s3.'+settings.AWS_S3_REGION_NAME+'.amazonaws.com/'+settings.AWS_STORAGE_BUCKET_NAME+'/' + str(obj.url)
        # input_path = 'https://bryant-esm.s3-us-west-1.amazonaws.com/20/70/70.mp4'

        cmd1 = ['ffmpeg',  '-i', str(input_path), '-ss' , '00:00:01.000' , '-vframes', '1' ,settings.MEDIA_ROOT+"/"+thumb_file]
        cmd2 = ['ffmpeg',  '-i', str(input_path), '-ss' , '00:00:01.000' , '-vframes', '1' ,settings.MEDIA_ROOT+"/"+main_file]

        try:
            retcode1 = subprocess.call(cmd1,shell=False)
            retcode2 = subprocess.call(cmd2,shell=False)

            media_storage = MediaRootS3Boto3Storage()
            s3_client = boto3.client('s3')
            file_path_within_bucket = 'media/'+thumb_file
            response = s3_client.upload_file(file_path_within_bucket, settings.AWS_STORAGE_BUCKET_NAME, file_path_within_bucket)
            
            file_path_within_bucket = 'media/'+main_file
            response = s3_client.upload_file(file_path_within_bucket, settings.AWS_STORAGE_BUCKET_NAME, file_path_within_bucket)

            os.remove(settings.MEDIA_ROOT+"/"+thumb_file)
            os.remove(settings.MEDIA_ROOT+"/"+main_file)

            obj.thumb = thumb_file
            obj.main = main_file 

            obj.save()
            data['id'] = obj.id
            data['saved'] = True

        except:
            obj.save()
            print("Thumb Not generated")
            data['id'] = obj.id
            data['saved'] = False
            
    return Response(data, status=status.HTTP_200_OK)   
         

# Photo Catgeoery
def add_photo_category(request):
    name = request.POST['add_category']
    activity = request.POST['get_activty_id']
    PhotoCategory.objects.create(name=name,activity_id=activity)
    return redirect('app:activity-edit',pk=activity)


class PhotoCategoryEditView(TemplateView):

        
    def get(self, request):

        form_class = PhotoCategoryForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            project_obj = PhotoCategory.objects.get(id=get_id)
            form = PhotoCategoryForm(instance=project_obj)
            return HttpResponse(form.as_p())


    def post(self,request):

        get_id = request.POST.get('get_photo_id')

        if get_id:
            project_obj = get_object_or_404(PhotoCategory, pk=get_id)
            if project_obj:
                form = PhotoCategoryForm(request.POST, instance=project_obj)
                if form.is_valid():
                    form.save()
                    response = {
                        'status': 'updated',
                        'ok': True
                    }
                    return JsonResponse(response)

        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error",
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)      



def PhotoCategoryDeleteView(request):

    get_id = request.POST['get_id']

    if get_id:

        PhotoCategory.objects.get(id=get_id).delete()
        response = {
                    'status': 'updated',
                    'ok': True
        }
        messages.success(request, "Deleted category successfully")

        return JsonResponse(response)

    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)    



# Video Catgeoery

def add_video_category(request):
    name = request.POST['video_category']
    activity = request.POST['get_activty_id']
    VideoCategory.objects.create(name=name,activity_id=activity)
    return redirect('app:activity-edit',pk=activity)


class VideoCategoryEditView(TemplateView):
        
    def get(self, request):

        form_class = VideoCategoryForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            project_obj = VideoCategory.objects.get(id=get_id)
            form = VideoCategoryForm(instance=project_obj)
            return HttpResponse(form.as_p())


    def post(self,request):

        get_id = request.POST.get('get_video_categ_id')

        if get_id:
            project_obj = get_object_or_404(VideoCategory, pk=get_id)
            if project_obj:
                form = VideoCategoryForm(request.POST, instance=project_obj)
                if form.is_valid():
                    form.save()
                    response = {
                        'status': 'updated',
                        'ok': True
                    }
                    return JsonResponse(response)

        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error",
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)      



def VideoCategoryDeleteView(request):

    get_id = request.POST['get_id']

    if get_id:

        VideoCategory.objects.get(id=get_id).delete()
        response = {
                    'status': 'updated',
                    'ok': True
        }
        messages.success(request, "Deleted category successfully")

        return JsonResponse(response)

    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)    


# Document cateory

       
def Ajax_upload_document(request):
    
    get_id = request.POST['get_activity_id']
    get_cat_id = request.POST['get_document_category_id']
    exist_document_file_url = request.POST['exist_document_file_url']

    if get_id and get_cat_id:
        actvity_obj = Activity.objects.get(id=get_id)
        category_obj = DocumentCategory.objects.get(id=get_cat_id)
        if exist_document_file_url:
            doc_object =Document()
            doc_object.name = get_filename_from_url(exist_document_file_url)
            doc_object.activity = actvity_obj
            doc_object.document_category = category_obj
            doc_object.save()
            upload_photo_s3_url_to_file(exist_document_file_url, doc_object)
            messages.success(request, "Uploaded document sudcessfully")
            return redirect('/activity/activity-edit/'+get_id+"?doc_categ="+get_cat_id)

        get_document = request.FILES.getlist('docuemnts')
        for ob in get_document:
            k=Document()
            k.name = ob.name
            k.activity = actvity_obj
            k.document_category = category_obj
            k.url=ob
            k.save()
        messages.success(request, "Uploaded document sudcessfully")
        return redirect('/activity/activity-edit/'+get_id+"?doc_categ="+get_cat_id)    
    else:
        messages.success(request, "Bad Request")
        return redirect('app:project-list')    


def add_document_category(request):
    name = request.POST['document_category']
    activity = request.POST['get_activty_id']
    DocumentCategory.objects.create(name=name,activity_id=activity)
    return redirect('app:activity-edit',pk=activity)


class DocumentCategoryFormEditView(TemplateView):
        
    def get(self, request):

        form_class = DocumentCategoryForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            project_obj = DocumentCategory.objects.get(id=get_id)
            form = DocumentCategoryForm(instance=project_obj)
            return HttpResponse(form.as_p())

    def post(self,request):

        get_id = request.POST.get('get_doc_categ_id')

        if get_id:
            project_obj = get_object_or_404(DocumentCategory, pk=get_id)
            if project_obj:
                form = DocumentCategoryForm(request.POST, instance=project_obj)
                if form.is_valid():
                    form.save()
                    response = {
                        'status': 'updated',
                        'ok': True
                    }
                    return JsonResponse(response)

        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error",
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)      


def DocumentCategoryDeleteView(request):

    get_id = request.POST['get_id']

    if get_id:
        DocumentCategory.objects.get(id=get_id).delete()
        response = {
                    'status': 'updated',
                    'ok': True
        }
        messages.success(request, "Deleted category successfully")
        return JsonResponse(response)
    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)










