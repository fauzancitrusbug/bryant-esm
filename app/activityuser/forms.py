# -*- coding: utf-8 -*-

from django import forms
from core.models import ActivityUser

# -----------------------------------------------------------------------------
# Project
# -----------------------------------------------------------------------------
 
class ActivityUserForm(forms.ModelForm):
 
    class Meta():
        model = ActivityUser
        fields = ['name','mobile','activity']

        
    def __init__(self, *args, **kwargs):
        super(ActivityUserForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class' : 'form-control'})

        self.fields['name'].widget.attrs['placeholder'] = 'Enter user name'   
        self.fields['mobile'].widget.attrs['placeholder'] = 'Enter mobile number'
        self.fields['name'].required = True
        self.fields['mobile'].required = True

   


 
