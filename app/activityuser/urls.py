# -*- coding: utf-8 -*-

from django.urls import path, include
from django.views.generic import TemplateView
from .views import ActivityUserView, ActivityUserCreateView, ActivityUserDeleteView,showphoto

urlpatterns = [

    path("show-photos",showphoto, name="showphoto"),
    path("activityuser-list",ActivityUserView.as_view(), name="activityuser-list"),
    path("activityuser-create",ActivityUserCreateView.as_view(), name="activityuser-create"),
    path("activityuser-delete",ActivityUserDeleteView, name="activityuser-delete"),

]

