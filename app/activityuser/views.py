
from django.views.generic import TemplateView
from core.models import ActivityUser, Activity,PhotoCategory,VideoCategory,DocumentCategory,Photo
from .forms import ActivityUserForm
from django.views import View
from http import HTTPStatus
from django.shortcuts import render, redirect,get_object_or_404, HttpResponse

from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.contrib import messages


class ActivityUserCreateView(TemplateView):

    model = ActivityUser
    form_class = ActivityUserForm

    def post(self, request):
        create_form = ActivityUserForm(request.POST)
        name = request.POST['name']
        mobile = request.POST['mobile']
        country_code = request.POST['country_code']
        activity = request.POST['activity']
        activity_obj = Activity.objects.get(id=activity)
        new_mobile = "{0}{1}".format(country_code,mobile)
        obj = ActivityUser.objects.create(name=name,mobile=new_mobile, activity=activity_obj)
        
        if obj:
                response = {
                'status': 'Created',
                'ok': True
                }

                messages.success(request, "Created User successfully")
                return JsonResponse(response)
        else:        
            response = {
                'status': 'Error while creating',
                'ok': False,
                'errors':""
            }
            return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)


         


class ActivityUserView(TemplateView):
    
    template_name = "frontend/activityuser/activityuser.html"

    def get(self, request):
        acivity_user = ActivityUser.objects.all()
        form_class = ActivityUserForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            acivity_user_obj = ActivityUser.objects.get(id=get_id)
            form = ActivityUserForm(instance=acivity_user_obj)
            return HttpResponse(form.as_p())
        else:

            context = {
                    "title": "ActivityUser",
                    "form":form_class,
                    "instance":acivity_user
            }
            return render(request, self.template_name,context)    
 
    def post(self,request):

        get_id = request.POST.get('get_active_user_id')

        if get_id:
            acivity_user_obj = get_object_or_404(ActivityUser, pk=get_id)
            if acivity_user_obj:
                form = ActivityUserForm(request.POST, instance=acivity_user_obj)
                if form.is_valid():
                    form.save()
                    response = {
                        'status': 'updated',
                        'ok': True
                    }

                    messages.success(request, "Updated User successfully")
                    return JsonResponse(response)

        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':""
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)

def showphoto(request):
    get_id = request.GET.get('get_id')
    obj = Photo.objects.filter(photo_category_id = get_id)
    return HttpResponse(obj)



   
def ActivityUserDeleteView(request):

    get_id = request.POST['get_id']

    if get_id:

        ActivityUser.objects.get(id=get_id).delete()
        response = {
                'status': 'updated',
                'ok': True
            }
            
        messages.success(request, "Deleted User successfully")
        return JsonResponse(response)

    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)      