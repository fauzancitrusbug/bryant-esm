# -*- coding: utf-8 -*-

from django import forms
from core.models import User

from core.utils import filter_perms

from django.contrib.auth.models import  Permission
from django.core.validators import RegexValidator


# -----------------------------------------------------------------------------
# Project
# -----------------------------------------------------------------------------
from django.db.models import Q


class MyUserForm(forms.ModelForm):

    class Meta():
        model = User
        fields = ['name','mobile','email','project','user_permissions', 'username']        

    def __init__(self, *args, **kwargs):
        super(MyUserForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['mobile'].widget.attrs['class'] = 'form-control'
        self.fields['project'].widget.attrs['class'] = 'form-control'
        self.fields['user_permissions'].widget.attrs['class'] = 'form-control'
        self.fields['user_permissions'].queryset = Permission.objects.filter(Q(codename__contains="project") | Q(codename__icontains="activity"))
        self.fields['name'].required = True
        self.fields['email'].required = True
        self.fields['mobile'].required = True
        self.fields['username'].required = False
        self.fields["username"].help_text = None
        self.fields["user_permissions"].help_text = None
        self.fields['username'].widget = forms.HiddenInput()






 

  
            
