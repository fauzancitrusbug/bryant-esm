# -*- coding: utf-8 -*-

from django.urls import path, include
from django.views.generic import TemplateView
from django.conf.urls import url
from .views import MyUserView,AdminCreateView,AdminDeleteView,AdminUserEditView,AdminEmail,change_password_admin,is_corrrect_password,UserPasswordView




urlpatterns = [

    path("admin-user-list/",MyUserView.as_view(), name="admin-user-list"),
    path("<int:pk>/password-change/", UserPasswordView.as_view(), name="password-change"),
    path("<int:pk>/change-admin-password/", change_password_admin, name="change-admin-password"),
    path("admin-create/",AdminCreateView.as_view(), name="admin-create"),
    path("admin-delete",AdminDeleteView, name="admin-delete"),
    path("edit-admin-user/<int:pk>",AdminUserEditView.as_view(), name="edit-admin-user"),
    #path("<uuid:uuid>",AdminEmail, name="welcome-list"),
    path("<int:pk>/password_check/", is_corrrect_password, name="password_check"),
    url(r'(?P<uuid>.+)',AdminEmail, name= 'welcome-admin'),

    #path("add-admin",AddAdmin, name="add-admin"),





]

