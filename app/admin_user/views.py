
from django.views.generic import TemplateView
from django.shortcuts import render, redirect,get_object_or_404, HttpResponse
from core.views.generic import MyPasswordUpdateView
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse

from core.models import Project, Activity,ActivityUser,User
from .forms import MyUserForm
from app.activity.forms import ActivityForm
from django.contrib import messages
from http import HTTPStatus
from django.core.mail import EmailMultiAlternatives
import uuid
from django.core import exceptions
import django.contrib.auth.password_validation as validators
from django.contrib.auth.hashers import check_password
from .email import Emails
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings


class MyUserView(TemplateView):
    
    template_name = "frontend/adminuser/adminuser.html"

    def get(self, request):
        users = User.objects.all()
        form_class = MyUserForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            user_obj = User.objects.get(id=get_id)
            form = MyUserForm(instance=user_obj)
            return HttpResponse(form.as_p())
        else:

            context = {
                    "title": "Admin Users",
                    "form":form_class,
                    "instance":users
            }
            return render(request, self.template_name,context)    
 
    def post(self,request):

        get_id = request.POST.get('get_project_id')

        if get_id:
            user_obj = get_object_or_404(User, pk=get_id)
            if user_obj:
                form = MyUserForm(request.POST, instance=user_obj)
                if form.is_valid():
                    form.save()
                    response = {
                        'status': 'updated',
                        'ok': True
                    }
                    return JsonResponse(response)

                response = {
                    'status': 'form_error',
                    'ok': False,
                    'errors':form.errors
                }
                return JsonResponse(response, status=HTTPStatus.OK)



class AdminCreateView(TemplateView):

    model = User
    form_class = MyUserForm

    def post(self, request):

        request.POST._mutable = True
        request.POST['username']=request.POST['email']
        create_form = MyUserForm(request.POST)
        
        if create_form.is_valid():
            user = create_form.save()
            response = {
            'status': 'Created',
            'ok': True
            }

            messages.success(request, "Created User successfully")

            current_site = settings.FRONT_URL
            user_obj = User.objects.get(email=user.email)
            token = user_obj.uuid
            user_obj.save()
            current_site = "{}/admin-users/{}".format(current_site, token)
            email = Emails(subject="Welcome !!!", recipient_list=user_obj.email,)
            email.set_html_message('welcome/user.html', {'name':user_obj.name,'current_site':current_site})
            email.send()
            return JsonResponse(response)

        else:

            response = {
                'status': 'form_error',
                'errors':create_form.errors
            }

            
            return JsonResponse(response, status=HTTPStatus.OK)
    


class AdminUserEditView(TemplateView):

    template_name = "frontend/adminuser/adminuser.html"    

    def get(self, request,pk):
        form_class = MyUserForm
        user_obj = Project.objects.get(pk=pk)
        user_form = MyUserForm(instance=user_obj)
  
        context = {
                "title": "Edit User",
                "form":form_class,
                "project_form":user_form,
                "instance":user_obj,
               
        }
        return render(request, self.template_name,context) 


   
def AdminDeleteView(request):

    get_id = request.POST['get_id']

    if get_id:

        User.objects.get(id=get_id).delete()
        response = {
                    'status': 'updated',
                    'ok': True
        }
        messages.success(request, "Deleted Admin User successfully")

        return JsonResponse(response)

    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)    



def change_password_admin(request,pk):
    if request.method=='POST':
        password1 = request.POST['password1']  
        password2 = request.POST['password2']
        if password1!=password2:
            message = "Both password did not match."
            return render(request,'frontend/adminuser/change-password.html',context={'message':message})
        else:
            user_obj = User.objects.get(pk = pk)
            user_obj.set_password(password1)
            user_obj.save()
            return redirect('app:sign-in')
    else:
        return render(request,'frontend/adminuser/change-password.html')


def AdminEmail(request,uuid):
    if request.method == 'POST':
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if password1!=password2:
            messages = "Both password did not match."
            return render(request,'frontend/registration.html',context={'email':email,'messages':messages,'uuid':uuid})
        else:
            user_obj = User.objects.get(email = email)
            user_obj.set_password(password1)
            user_obj.save()
            return redirect('app:sign-in')


    else:
        user_obj = User.objects.get(uuid = uuid)
        email = user_obj.email
        return render(request,'frontend/registration.html',context={'email':email})


class UserPasswordView(MyPasswordUpdateView):
    """View to change User Password"""

    model = User
    form_class = AdminPasswordChangeForm
    template_name = "frontend/adminuser/change-password.html"
    permission_required = ("core.change_user",)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # kwargs['user'] = self.request.user
        kwargs["user"] = kwargs.pop("instance")
        return kwargs
    
 

   
        

@csrf_exempt
def is_corrrect_password(request,pk):
    if request.method == 'POST':
        print("Ajax is called ! !")
        password = request.POST['password']
        admin_obj = User.objects.filter(pk=pk).first()
        print(admin_obj.password)
        print(admin_obj.check_password(password))
        if admin_obj.check_password(password):
            return HttpResponse('correct')
        else:
            return HttpResponse('wrong')       