# -*- coding: utf-8 -*-

from django import forms
from core.models import Project

# -----------------------------------------------------------------------------
# Project
# -----------------------------------------------------------------------------


class ProjectForm(forms.ModelForm):
 
    """Custom UserCreationForm."""

    class Meta():
        model = Project
        fields = ['title','status']

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['title'].widget.attrs['placeholder'] = 'Enter project name'   
        self.fields['status'].widget.attrs['class'] = 'form-control'
        self.fields['title'].required = True


 

  
            
