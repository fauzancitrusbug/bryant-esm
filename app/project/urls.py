# -*- coding: utf-8 -*-

from django.urls import path, include
from django.views.generic import TemplateView

from .views import ProjectView, ProjectCreateView, ProjectDeleteView, ProjectsActivityView




urlpatterns = [

    path("project-list",ProjectView.as_view(), name="project-list"),
    path("project-create",ProjectCreateView.as_view(), name="project-create"),
    path("project-delete",ProjectDeleteView, name="project-delete"),
    path("project-actvity/<int:pk>",ProjectsActivityView.as_view(), name="project-actvity"),




]

