
from django.views.generic import TemplateView
from django.shortcuts import render, redirect,get_object_or_404, HttpResponse

from django.http import JsonResponse, HttpResponseRedirect, HttpResponse

from core.models import Project, Activity,ActivityUser,User
from .forms import ProjectForm
from app.activity.forms import ActivityForm
from django.contrib import messages
from http import HTTPStatus


class ProjectView(TemplateView):
    
    template_name = "frontend/project/all-projects.html"

    def get(self, request):
        project = Project.objects.all().order_by("-created_at")
        form_class = ProjectForm

        if request.is_ajax():
            get_id = request.GET.get('get_id')
            project_obj = Project.objects.get(id=get_id)
            form = ProjectForm(instance=project_obj)
            return HttpResponse(form.as_p())
        else:
            user_obj = User.objects.filter(email = request.user.email).first()
            context = {
                    "title": "Project",
                    "form":form_class,
                    "instance":project,
                    "user_obj":user_obj
            }
            return render(request, self.template_name,context)    
 
    def post(self,request):

        get_id = request.POST.get('get_project_id')

        if get_id:
            project_obj = get_object_or_404(Project, pk=get_id)
            if project_obj:
                form = ProjectForm(request.POST, instance=project_obj)
                if form.is_valid():
                    form.save()
                    response = {
                        'status': 'updated',
                        'ok': True
                    }
                    return JsonResponse(response)

        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':form.errors
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)



class ProjectCreateView(TemplateView):

    model = Project
    form_class = ProjectForm

    def post(self, request):
        create_form = ProjectForm(request.POST)
        if create_form.is_valid():
            ob=create_form.save(request)
            messages.success(request, "Created {0} project successfully".format(ob.title))
            
            user=User.objects.filter(id=request.user.id).first()
            user.project.add(ob)

            return redirect('app:project-list')



class ProjectsActivityView(TemplateView):

    template_name = "frontend/activity/all-activity.html"    

    def get(self, request,pk):
        acivity_obj=Activity.objects.filter(project=pk)
        form_class = ActivityForm
        project_obj = Project.objects.get(pk=pk)
        project_form = ProjectForm(instance=project_obj)
  
        context = {
                "title": "Activity",
                "form":form_class,
                "project_form":project_form,
                "instance":acivity_obj,
                "project_id":pk,
        }
        return render(request, self.template_name,context) 


   
def ProjectDeleteView(request):

    get_id = request.POST['get_id']

    if get_id:

        Project.objects.get(id=get_id).delete()
        response = {
                    'status': 'updated',
                    'ok': True
        }
        messages.success(request, "Deleted project successfully")

        return JsonResponse(response)

    else:
        response = {
            'status': 'Error while updating',
            'ok': False,
            'errors':"error"
        }
        return JsonResponse(response, status=HTTPStatus.BAD_REQUEST)    





    