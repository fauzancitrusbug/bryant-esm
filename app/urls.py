# -*- coding: utf-8 -*-

from django.urls import path, include
from django.views.generic import TemplateView

from .views import SignIn, LogoutView
from django.urls import path,re_path
from django.conf.urls import url

from . import views 

from .activity.views import Ajax_upload_videos
from .activity.views import Ajax_update_videos


app_name = "app"

urlpatterns = [


    path("",SignIn.as_view(), name="sign-in"),
    path("",SignIn.as_view(), name="logout"),

    path("project/",include('app.project.urls')),
    path("activityuser/",include('app.activityuser.urls')),
    path("activity/",include('app.activity.urls')),
    path("admin-users/",include('app.admin_user.urls')),

    path("api/files/policy/", Ajax_upload_videos, name='upload-policy'),
    path("api/files/policy/", Ajax_upload_videos, name='upload-policy'),
    path('api/files/complete/', Ajax_update_videos, name='upload-complete'),

    
]

