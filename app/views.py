
from django.views.generic import TemplateView
from django.shortcuts import render, redirect,get_object_or_404
from core.models import User
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate




ErrorMsg ={
    'email_required':'Email is required.',
    'user_not_exists':'User not exists',
    'invalid_password':'Invalid password',
    'not_staff':'User not admin staff',
    'inactive_user':'Inactive user',
}



class SignIn(TemplateView):
    
    template_name = "frontend/sign-in.html"

    context = {
        "title": "Sign-in",
    }

    def get(self, request):
        return render(request, self.template_name, self.context)    

    def post(self, request):

        email =  request.POST['email']
        password =  request.POST['password']

        if not email:
            messages.success(request, ErrorMsg['email_required'])

        user = User.objects.filter(email=email).distinct()
        user = user.exclude(email__isnull=True).exclude(email__iexact='')

        if user.exists() and user.count() ==1:
            user_obj=user.first()
        else:
            messages.success(request, ErrorMsg['user_not_exists'])
            return redirect('app:sign-in')

        if user_obj and not user_obj.check_password(password):
            messages.success(request, ErrorMsg['invalid_password'])
            return redirect('app:sign-in')

        if user_obj.is_active:
            login(request, user_obj, backend='django.contrib.auth.backends.ModelBackend')

            messages.success(request, "Welcome {0}".format(user_obj.username))
            return redirect('app:project-list')
        else:
            messages.success(request, ErrorMsg['inactive_user'])
            return redirect('app:sign-in')    

        
        
class LogoutView(TemplateView):

    def get(self, request):
        logout(request)
        return redirect('app:sign-in')