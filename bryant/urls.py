from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token

admin.site.site_title = "Bryant ESM Engineer"
admin.site.site_header = "Bryant ESM"
admin.site.index_title = "Site Administration"

from django.urls import path,re_path
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from django.contrib.auth.forms import PasswordResetForm
from core.models import User
from django.core.exceptions import ValidationError

from core.api.views.play_store_redirect import QrCodeScanRedirectView


class EmailValidationOnForgotPassword(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email).exists():
            raise ValidationError("There is no user registered with the specified email address!")

        return email



urlpatterns = [
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name="frontend/password_reset_form.html",form_class=EmailValidationOnForgotPassword, html_email_template_name="registration/password_reset_email.html"), name='password_reset',),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name="frontend/password_reset_complete.html"), name='password_reset_complete'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name="frontend/password_reset_done.html"),  name='password_reset_done'),
    re_path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/', auth_views.PasswordResetConfirmView.as_view(template_name="frontend/password_reset_confirm.html"), name='password_reset_confirm'),
    path("", include("app.urls")),
    path("home/", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    path(
        "about/", TemplateView.as_view(template_name="pages/about.html"), name="about"
    ),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    path("customadmin/", include("core.urls")),
    path("qrcode-scan-redirect/<str:url>", QrCodeScanRedirectView.as_view(), name="qrcode-scan-redirect"),
    
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# # API URLS
urlpatterns += [
    # API base url
    path("api/v1/", include("bryant.api_router")),
    # DRF auth token
    #path("auth-token/", obtain_auth_token),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
        
      
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
