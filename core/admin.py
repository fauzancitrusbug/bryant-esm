from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from core.models import User, Activity, ActivityUser, Photo, Video,Document, Project,PhotoCategory,VideoCategory,DocumentCategory


from django.contrib.auth.models import Permission

admin.site.register(Permission)


class UserAdmins(admin.ModelAdmin):
    list_display =['username','mobile','otp_verified']

 
admin.site.register(User, UserAdmins)
admin.site.register(Activity)
admin.site.register(ActivityUser)
admin.site.register(Photo)
admin.site.register(Document)
admin.site.register(Video)

admin.site.register(Project)
admin.site.register(PhotoCategory)
admin.site.register(VideoCategory)
admin.site.register(DocumentCategory)




