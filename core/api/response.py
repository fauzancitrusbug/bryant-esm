
from rest_framework import status
from rest_framework.response import Response



def MyAPIResponse(data=None, code=status.HTTP_200_OK):

    """Custom API response format."""
    
    return Response(data,status=code)
