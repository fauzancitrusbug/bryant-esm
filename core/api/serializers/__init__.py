from .register import RegisterUserSerializer, OtpSerializer
from .project import ProjectSerializer
from .activity import ActivitySerializer
from .activity_user import ActivityUserSerializer
from .activity import ActivitySerializer, ActivityListSerializer
from .get_activity_details import ActivityNewListSerializer

