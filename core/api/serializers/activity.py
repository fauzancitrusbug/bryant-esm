from rest_framework import serializers

from core.models import Activity, Photo, Document, Video
# -----------------------------------------------------------------------------
# Activity serializers
# -----------------------------------------------------------------------------
from pathlib import Path
from django.conf import settings
import pyqrcode
from sorl.thumbnail import get_thumbnail
from PIL import Image
from io import BytesIO


class PhotoSerializers(serializers.ModelSerializer):
    activity = serializers.CharField(required=False, write_only=True)
   
    class Meta:

        model = Photo
        fields = ("id","name","url","activity","date","thumb","main","discription")
        extra_kwargs = {
            'thumb': {'read_only': True},
            'main': {'read_only': True},
            }
  

class VideoSerializers(serializers.ModelSerializer):
    activity = serializers.CharField(required=False, write_only=True)
    url = serializers.SerializerMethodField('get_video_url')

    class Meta:

        model = Video
        fields = ("id","name","url","activity","date","thumb","main")
        
        extra_kwargs = {
            'thumb': {'read_only': True},
            'main': {'read_only': True},
            }


    def get_video_url(self,obj):
        url = "{0}{1}".format(settings.AWS_BUCKET_URL,obj.url)
        return url    

class DocumentSerializers(serializers.ModelSerializer):
    activity = serializers.CharField(required=False, write_only=True)

    class Meta:

        model = Document
        fields = ("id","name","url","activity","date","thumb","main")

        extra_kwargs = {
            'thumb': {'read_only': True},
            'main': {'read_only': True},
            }



class ActivitySerializer(serializers.ModelSerializer):
    photos =  PhotoSerializers(many=True,required=False)
    videos = VideoSerializers(many=True, required=False)
    documents = DocumentSerializers(many=True, required=False)
    qr_code = serializers.SerializerMethodField('get_qr_code')

    """Serializes the User data into JSON"""

    class Meta:

        model = Activity
        fields = (
            "id",
            "title",
            "status", 
            "discription",
            "qr_code",
            "photos",
            "videos",
            "documents"
        )
       
    def create(self, validated_data):
        photos = validated_data.pop('photos',None)
        videos = validated_data.pop('videos',None)
        documents = validated_data.pop('documents',None)
        activity = Activity.objects.create(**validated_data)

        request = self.context.get('request')
        url = str(request.data.get('url', ''))+'/'+str(activity.id)
        data = str(activity.id)
        qr = pyqrcode.create(url)
        name = '/qr_code/'+data+'.png'
        Path(settings.MEDIA_ROOT+"/qr_code/").mkdir(parents=True, exist_ok=True)
        qr.png(settings.MEDIA_ROOT+name , scale=8)
        activity.qr_code = name
        activity.save()
        
        if photos is not None:
            for img in photos:
                image = img.pop('url')                         
                ob = Photo.objects.create(activity=activity,name=image,url=image)
                 

        if videos is not None:
            for vid in videos:
                video = vid.pop('url')
        
                ob=Video.objects.create(activity=activity,name=video,url=video)
                

        if documents is not None:   
            for doc in documents:
                document = doc.pop('url')
                Document.objects.create(activity=activity,name=document, url=document)  

        return activity


    def update(self, instance, validated_data):

        instance = super().update(instance, validated_data)

        if 'image' in validated_data.keys():
            image = validated_data.pop('image')
            for img in image:
                image = img.pop('image')
                Photo.objects.create(activity=activity,image=image)

        if 'video' in validated_data.keys():
            video = validated_data.pop('video')
            for  vid in video:
                video = vid.pop('video')
                Video.objects.create(activity=activity,video=video)        

        if 'documents' in validated_data.keys():
            documents = validated_data.pop('documents')
            for doc in documents:
                Document.objects.create(activity=activity,document=doc)        
        
        return instance  

    def get_qr_code(self, instance):
    
        request = self.context.get('request')

        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print(instance.qr_code.url)
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')


        if request:
            return request.build_absolute_uri(instance.qr_code.url)
        return instance.qr_code.url    


    

class ActivityListSerializer(serializers.ModelSerializer):
    photos = serializers.SerializerMethodField("get_activity_photos")
    videos = serializers.SerializerMethodField("get_activity_videos")
    documents = serializers.SerializerMethodField("get_activity_documents")


    """Serializes the Activity data into JSON"""

    class Meta:

        model = Activity
        fields = (
            "id",
            "title",
            "status", 
            "discription",
            "photos",
            "videos",
            "documents"
        )

    def get_activity_photos(self,obj):
        request = self.context.get("request")
        activity_photo = Photo.objects.filter(activity=obj.id)
        serializer = PhotoSerializers(activity_photo, many=True,context={'request': request})
        return serializer.data    

    def get_activity_videos(self,obj):
        request = self.context.get("request")
        activity_video = Video.objects.filter(activity=obj.id)
        serializer = VideoSerializers(activity_video, many=True,context={'request': request})
        return serializer.data 

    def get_activity_documents(self,obj):
        request = self.context.get("request")
        activity_doc = Document.objects.filter(activity=obj.id)
        serializer = DocumentSerializers(activity_doc, many=True,context={'request': request})
        return serializer.data     