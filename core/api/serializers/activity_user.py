from rest_framework import serializers

from core.models import ActivityUser
# -----------------------------------------------------------------------------
# ActivityUser serializers
# -----------------------------------------------------------------------------

class ActivityUserSerializer(serializers.ModelSerializer):

    """Serializes the ActivityUser data into JSON"""

    class Meta:

        model = ActivityUser
        fields = (
            "id",
            "name",
            "mobile", 
            "activity"
        )
  