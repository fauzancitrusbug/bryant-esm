from rest_framework import serializers

from core.models import Activity, Photo, Document, Video, PhotoCategory, VideoCategory, DocumentCategory

from .activity import PhotoSerializers, DocumentSerializers, VideoSerializers

# -----------------------------------------------------------------------------
# Activity serializers
# -----------------------------------------------------------------------------


class ActivityNewListSerializer(serializers.ModelSerializer):
    photos = serializers.SerializerMethodField("get_activity_photos")
    videos = serializers.SerializerMethodField("get_activity_videos")
    documents = serializers.SerializerMethodField("get_activity_documents")
    total_photo = serializers.SerializerMethodField("get_toatl_photo")
    total_video = serializers.SerializerMethodField("get_total_video")
    total_document = serializers.SerializerMethodField("get_total_document")



    """Serializes the Activity data into JSON"""

    class Meta:

        model = Activity
        fields = (
            "id",
            "title",
            "status", 
            "discription",
            "total_photo",
            "total_video",
            "total_document",
            "photos",
            "videos",
            "documents"
        )

    def get_activity_photos(self,obj):
        request = self.context.get("request")
   
        res=[]
        img_cat = PhotoCategory.objects.filter(activity=obj)
        for k in img_cat:   
            data={}        
            img_instance = Photo.objects.filter(activity=obj, photo_category__id=k.id)
            if img_instance:
                serializer = PhotoSerializers(img_instance, many=True,context={'request': request})
                data['title']=k.name
                data["data"]= serializer.data
                res.append(data)

        return res   

    def get_activity_videos(self,obj):
        request = self.context.get("request")

        res=[]
        video_cat = VideoCategory.objects.filter(activity=obj)
        for k in video_cat:   
            data={}        
            video_instance = Video.objects.filter(activity=obj, video_category__id=k.id)
            if video_instance:
                serializer = VideoSerializers(video_instance, many=True,context={'request': request})

                data['title']=k.name
                data["data"]= serializer.data
                res.append(data)
        return res   
    

    def get_activity_documents(self,obj):

        request = self.context.get("request")
        res=[]
        doc_cat = DocumentCategory.objects.filter(activity=obj)

        for k in doc_cat:   
            data={}        
            doc_instance = Document.objects.filter(activity=obj, document_category__id=k.id)
            if doc_instance:
                serializer = DocumentSerializers(doc_instance, many=True,context={'request': request})
                data['title']=k.name
                data["data"]= serializer.data
                res.append(data)

        return res   



    def get_toatl_photo(self,obj):
        return Photo.objects.filter(activity=obj).count()

   
    def get_total_video(self,obj):
        return Video.objects.filter(activity=obj).count()


    def get_total_document(self,obj):
        return Document.objects.filter(activity=obj).count()