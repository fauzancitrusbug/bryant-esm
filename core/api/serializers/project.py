from rest_framework import serializers

from core.models import Project
# -----------------------------------------------------------------------------
# Project serializers
# -----------------------------------------------------------------------------

class ProjectSerializer(serializers.ModelSerializer):

    """Serializes the User data into JSON"""

    class Meta:

        model = Project
        fields = (
            "id",
            "title",
            "status", 
        )
       