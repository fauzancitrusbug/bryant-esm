from rest_framework import serializers

from core.models import User
# -----------------------------------------------------------------------------
# User serializers
# -----------------------------------------------------------------------------

class RegisterUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField()


    """Serializes the User data into JSON"""

    class Meta:

        model = User
        fields = (
            "id",
            "name",
            "mobile", 
            "code", 
            "sent_time", 
            "username"
        )
        extra_kwargs = {
            'name': {'write_only': True},
            'sent_time': {'write_only': True},
            'username': {'write_only': True},
            }
        
class OtpSerializer(serializers.ModelSerializer):
    api_token = serializers.ReadOnlyField(required=False)

    """Serializes the User data into JSON"""

    class Meta:

        model = User
        fields = (
            "id",
            "mobile", 
            "name",
            "api_token",
        )
        
