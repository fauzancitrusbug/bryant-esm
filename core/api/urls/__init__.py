# -*- coding: utf-8 -*-
from django.urls import include, path 
from . import register
from .import verifycode
from .import resendcode
from .import project
from . import activity_user
from .import activity
from .import scancode
from . import getactivitydetail
from core.api.views.get_Activity import GetActivityNewDetailsAPIView

urlpatterns = [
     
    path("register/", include(register)),
    
    path("verifycode/", include(verifycode)),

    path("resendcode/", include(resendcode)),

    path("project/", include(project)),

    path("activity-user/", include(activity_user)),

    path("activity/", include(activity)),

    path("scancode/", include(scancode)),

    path("getactivitydetail/", include(getactivitydetail)),
    
    path("v2/getactivitydetail/", GetActivityNewDetailsAPIView.as_view()),


]
