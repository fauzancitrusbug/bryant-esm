from django.urls import include, path
from core.api.views.activity import (
    ActivityAPIView,
    ActivityCreateAPI,
    ActivityDetailsAPIView,
    ActivityUpdateAPIView,
    ActivityDeleteAPIView,
) 

urlpatterns = [
    
    path("",ActivityAPIView.as_view(),name="activity-list"),
    path("create/",ActivityCreateAPI.as_view(),name="activity-create"),
    path('details/<int:pk>', ActivityDetailsAPIView.as_view(), name='activity-details'),
    path('update/<int:pk>', ActivityUpdateAPIView.as_view(), name='activity-update'),
    path('delete/<int:pk>', ActivityDeleteAPIView.as_view(), name='activity-delete'),
]
 