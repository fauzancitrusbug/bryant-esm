from django.urls import include, path
from core.api.views.activity_user import (
    ActivityUserAPIView,
    ActivityUserCreateAPI,
    ActivityUserDetailsAPIView,
    ActivityUserUpdateAPIView,
    ActivityUserDeleteAPIView, 
) 

urlpatterns = [
    
    path("",ActivityUserAPIView.as_view(),name="activity-user-list"),
    path("create/",ActivityUserCreateAPI.as_view(),name="activity-user-create"),
    path('details/<int:pk>', ActivityUserDetailsAPIView.as_view(), name='activity-user-details'),
    path('update/<int:pk>', ActivityUserUpdateAPIView.as_view(), name='activity-user-update'),
    path('delete/<int:pk>', ActivityUserDeleteAPIView.as_view(), name='activity-user-delete'),
]
 