from django.urls import include, path
from core.api.views.project import (
    ProjectAPIView,
    ProjectCreateAPI,
    ProjectDetailsAPIView,
    ProjectUpdateAPIView,
    ProjectDeleteAPIView, 
) 

urlpatterns = [
    
    path("",ProjectAPIView.as_view(),name="project-list"),
    path("create/",ProjectCreateAPI.as_view(),name="project-create"),
    path('details/<int:pk>', ProjectDetailsAPIView.as_view(), name='project-details'),
    path('update/<int:pk>', ProjectUpdateAPIView.as_view(), name='project-update'),
    path('delete/<int:pk>', ProjectDeleteAPIView.as_view(), name='project-delete'),
]
 