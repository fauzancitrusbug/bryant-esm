from django.urls import include, path
from core.api.views.register import (
    RegisterUserApiView,    
) 

urlpatterns = [

    path("",RegisterUserApiView.as_view(),name="register"),
]
 