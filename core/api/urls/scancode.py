from django.urls import include, path
from core.api.views.scancode import (
    ScanCodeAPI,    
) 

urlpatterns = [

    path("",ScanCodeAPI.as_view(),name="scancode"),
]
 