from django.urls import include, path
from core.api.views.register import (
    UserOtpVerificationAPIView
    
) 

urlpatterns = [

    path("",UserOtpVerificationAPIView.as_view(),name="verifycode"),

]
 