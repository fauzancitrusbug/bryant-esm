
from .register import RegisterUserApiView, UserOtpVerificationAPIView, ResendCodeApi

from .project import ProjectAPIView, ProjectCreateAPI, ProjectDetailsAPIView, ProjectUpdateAPIView, ProjectDeleteAPIView


from .activity_user import (
    ActivityUserAPIView,
    ActivityUserCreateAPI,
    ActivityUserDetailsAPIView,
    ActivityUserUpdateAPIView,
    ActivityUserUpdateAPIView,
)


from .activity import (
    ActivityAPIView,
    ActivityCreateAPI,
    ActivityDetailsAPIView,
    ActivityUpdateAPIView,
    ActivityDeleteAPIView,
)

from .scancode import ScanCodeAPI

from .getactivitydetail import GetActivityDetailsAPIView