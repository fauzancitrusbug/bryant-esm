from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
from core.api.pagination import CustomPagination

from rest_framework.permissions import IsAuthenticated, AllowAny

from core.api.serializers import ActivitySerializer

from core.models.activity import Activity
from core.models.media_file_models import Photo, Video, Document

# .................................................................................
# Activity API
# .................................................................................


class ActivityAPIView(generics.ListAPIView):
    """API View for Activity listing"""

    permission_classes = (AllowAny,)
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    pagination_class = CustomPagination


class ActivityCreateAPI(APIView):

    """API View to create Activity"""

    permission_classes = (AllowAny,)
    serializer_class = ActivitySerializer
    queryset = Activity.objects.all()

    def post(self, request, format=None):

        """POST method to create the data"""

        if request.user.is_authenticated:

            try:
                serializer = ActivitySerializer(data=request.data,  context={'request':request})
                if serializer.is_valid():
                    ob=serializer.save()

                    return Response({
                        "status": True,
                        "code" : status.HTTP_201_CREATED,
                        "message" : "Successfully created activity",
                        "data": serializer.data,

                    }, status=status.HTTP_201_CREATED) 
                        
                else: 
                    return Response({
                                "status": False,
                                "code" : status.HTTP_200_OK,
                                "message" : "Cannot create activity",
                                "data": serializer.errors,

                                }, status=status.HTTP_200_OK)
            except:
                return Response({
                    "status": False,
                    "code" : status.HTTP_400_BAD_REQUEST,
                    "message" : "Bad request",
                    "data":{}
                    }, status=status.HTTP_400_BAD_REQUEST)

        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{}
                }, status=status.HTTP_401_UNAUTHORIZED)


class ActivityDetailsAPIView(APIView):

    """API View for Activity details"""

    permission_classes = (AllowAny,)

    def get(self, request, pk,  format=None):

        """GET method for retrieving the data"""

        try:
            activity = Activity.objects.get(pk=pk)      
            if activity is not None:                   
                serializer = ActivitySerializer(activity, context={"request": request})
                return Response({
                  
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Successfully fetched activity details",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
                    
        except Activity.DoesNotExist:
            return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Activity not found",
                    "data": {},
                    }, status=status.HTTP_404_NOT_FOUND)


class ActivityDeleteAPIView(APIView):

    """API View to delete Activity"""

    permission_classes = (AllowAny,)

    def delete(self, request, pk, format=None):
        """DELETE method to delete the data"""

        if request.user.is_authenticated:
            try:
                activity = Activity.objects.get(pk=pk)      
                activity.delete()
                return Response({
                        "status": True,
                        "code" : status.HTTP_200_OK,
                        "message" : "Successfully deleted activity",
                        "data":{}
                        }, status=status.HTTP_200_OK)

            except Activity.DoesNotExist:
                return Response({
                  
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Activity not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)
        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{}
                }, status=status.HTTP_401_UNAUTHORIZED)


class ActivityUpdateAPIView(APIView):

    """API View to update existing Activity"""

    permission_classes = (AllowAny,)
    serializer_class = ActivitySerializer

    def put(self, request, pk, format=None):

        """PUT method to update the data"""

        if request.user.is_authenticated:
           
            activity_check = Activity.objects.filter(pk=pk).exists()  
            if not activity_check:
                return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Activity not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)

            if "remove_photos" in request.data.keys():
                remove_photos = request.data.getlist('remove_photos')
                for rm_img in remove_photos:
                    ob = Photo.objects.filter(id=rm_img).exists()
                    if ob:
                        Photo.objects.get(id=rm_img).delete()
                    else:
                        return Response({
                            "status": False,
                            "code" : status.HTTP_404_NOT_FOUND,
                            "message" : "Photo Id not exist",
                            "data":{}
                            }, status=status.HTTP_404_NOT_FOUND) 

            
            if "remove_documents" in request.data.keys():
                remove_documents = request.data.getlist('remove_documents')
                for rm_doc in remove_documents:
                    ob = Document.objects.filter(id=rm_doc).exists()
                    if ob:
                        Document.objects.get(id=rm_doc).delete()
                    else:
                        return Response({
                            "status": False,
                            "code" : status.HTTP_404_NOT_FOUND,
                            "message" : "Document Id not exist",
                            "data":{},
                            }, status=status.HTTP_404_NOT_FOUND)      

            
            if "remove_videos" in request.data.keys():
                remove_videos = request.data.getlist('remove_videos')
                for rm_video in remove_videos:
                    ob = Video.objects.filter(id=rm_video).exists()
                    if ob:
                        Video.objects.get(id=rm_video).delete()
                    else:
                        return Response({
                            "status": False,
                            "code" : status.HTTP_404_NOT_FOUND,
                            "message" : "Video Id not exist",
                            "data":{},
                            }, status=status.HTTP_404_NOT_FOUND)                             


            activity = Activity.objects.get(pk=pk)

            serializer = ActivitySerializer(activity, data=request.data,partial=True, context={"request": request})
            
            if serializer.is_valid():
                obj=serializer.save()
              
                return Response({
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Successfully updated activity",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
            else:
                return Response({
                   
                    "status": False,
                    "code" : status.HTTP_200_OK,
                    "message" : "Activity serializer error",
                    "data": serializer.errors,
                    }, status=status.HTTP_200_OK)      
        else: 
            return Response({
            
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{}
                }, status=status.HTTP_401_UNAUTHORIZED)