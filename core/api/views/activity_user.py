from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from core.api.pagination import CustomPagination

from rest_framework.permissions import IsAuthenticated, AllowAny

from core.models.activity_user import ActivityUser
from core.api.serializers import ActivityUserSerializer

# .................................................................................
# ActivityUser API
# .................................................................................


class ActivityUserAPIView(generics.ListAPIView):

    """API View for ActivityUser listing"""

    permission_classes = (IsAuthenticated,)
    queryset = ActivityUser.objects.all()
    serializer_class = ActivityUserSerializer
    pagination_class = CustomPagination


class ActivityUserCreateAPI(APIView):

    """API View to create ActivityUser """

    permission_classes = (AllowAny,)
    serializer_class = ActivityUserSerializer

    def post(self, request, format=None):

        """POST method to create the data"""

        if request.user.is_authenticated:
            try:
                serializer = ActivityUserSerializer(data=request.data,  context={"request": request})
                if serializer.is_valid():
                    instance = serializer.save()
                    return Response({
                        "status": True,
                        "code" : status.HTTP_201_CREATED,
                        "message" : "Successfully created ",
                        "data": serializer.data,
                    }, status=status.HTTP_201_CREATED) 
                        
                else: 
                    return Response({
                        "status": False,
                        "code" : status.HTTP_200_OK,
                        "message" : "Cannot create",
                        "data": serializer.errors,
                        }, status=status.HTTP_200_OK)
            except:
                return Response({
                    "status": False,
                    "code" : status.HTTP_400_BAD_REQUEST,
                    "message" : "Bad request",
                    "data": {},
                    }, status=status.HTTP_400_BAD_REQUEST)

        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data": {},
                }, status=status.HTTP_401_UNAUTHORIZED)


class ActivityUserDetailsAPIView(APIView):
    
    """API View for Group feed details"""

    permission_classes = (IsAuthenticated,)

    def get(self, request, pk,  format=None):

        """GET method for retrieving the data"""

        try:
            activity = ActivityUser.objects.get(pk=pk) 

            if activity is not None:                   
                serializer = ActivityUserSerializer(activity, context={"request": request})
                return Response({
                   
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Successfully fetched activity user deatils",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
                    
        except ActivityUser.DoesNotExist:
            return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Activity user not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)


class ActivityUserDeleteAPIView(APIView):

    """API View to delete ActivityUser"""

    permission_classes = (IsAuthenticated,)

    def delete(self, request, pk, format=None):

        """DELETE method to delete the data"""

        if  request.user.is_authenticated:
            try:
                activity = ActivityUser.objects.get(pk=pk)      
                activity.delete()
                return Response({
                        "status": True,
                        "code" : status.HTTP_200_OK,
                        "message" : "Successfully deleted activity user",
                        "data":{}
                        }, status=status.HTTP_200_OK)

            except ActivityUser.DoesNotExist:
                return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "ActivityUser not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)
        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{}
                }, status=status.HTTP_401_UNAUTHORIZED)


class ActivityUserUpdateAPIView(APIView):

    """API View to update existing Project"""

    permission_classes = (AllowAny,)
    serializer_class = ActivityUserSerializer

    def put(self, request, pk, format=None):

        """PUT method to update the data"""

        if  request.user.is_authenticated:
           
            activity_user = ActivityUser.objects.filter(pk=pk).exists()  
            if not activity_user:
                return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Activity user not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)

        
            activity_user = ActivityUser.objects.get(pk=pk) 
            serializer = ActivityUserSerializer(activity_user, data=request.data,partial=True, context={"request": request})
            
            if serializer.is_valid():
                serializer.save()
                return Response({
                    
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Successfully updated activity user",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "status": False,
                    "code" : status.HTTP_200_OK,
                    "message" : "Activity user serializer validation error",
                    "data": serializer.errors,
                    }, status=status.HTTP_200_OK)      
    
        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{},
                }, status=status.HTTP_401_UNAUTHORIZED)                

