from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework.permissions import IsAuthenticated, AllowAny

from core.api.serializers import ActivityNewListSerializer

from core.models.activity import Activity
from core.models.media_file_models import Photo, Video, Document



class GetActivityNewDetailsAPIView(APIView):

    """API View for Activity details"""

    permission_classes = (AllowAny,)
    queryset = Activity.objects.all()
    serializer_class = ActivityNewListSerializer

    def get(self, request, format=None):

        if request.user.is_authenticated:
                pk=request.GET['id']

                activity = Activity.objects.get(pk=int(pk))
                serializer = ActivityNewListSerializer(activity, context={"request": request})

                return Response({
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Detail fetched successfully",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
         
        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_400_BAD_REQUEST,
                "message" : "Unauthorised User",
                "data":{}
                }, status=status.HTTP_400_BAD_REQUEST)
