
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect, HttpResponse
from django.views import View
from django.conf import settings


@method_decorator(csrf_exempt, name='dispatch')
class QrCodeScanRedirectView(View):
    def get(self, request, *args, **kwargs):
        HTTP_USER_AGENT = request.META['HTTP_USER_AGENT'].lower()    
        if "android" in HTTP_USER_AGENT:
            return HttpResponseRedirect(settings.PLAYSTORE)
        elif "iphone" in HTTP_USER_AGENT:
            return HttpResponseRedirect(settings.APPLESTORE)
        else:
           return HttpResponseRedirect('http://{}'.format(settings.FRONT_URL))

           
    

