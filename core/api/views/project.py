from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from core.api.pagination import CustomPagination

from rest_framework.permissions import IsAuthenticated, AllowAny

from core.models.project import Project
from core.api.serializers import ProjectSerializer

# .................................................................................
# Project API
# .................................................................................


class ProjectAPIView(generics.ListAPIView):

    """API View for Project listing"""

    permission_classes = (IsAuthenticated,)
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    pagination_class = CustomPagination


class ProjectCreateAPI(APIView):

    """API View to create Project """

    permission_classes = (IsAuthenticated,)
    serializer_class = ProjectSerializer

    def post(self, request, format=None):

        """POST method to create the data"""

        if  request.user.is_authenticated:
            try:
                serializer = ProjectSerializer(data=request.data,  context={"request": request})
                if serializer.is_valid():
                    instance = serializer.save()
                    return Response({
                        "status": True,
                        "code" : status.HTTP_201_CREATED,
                        "message" : "Successfully created project ",
                        "data": serializer.data,
                    }, status=status.HTTP_201_CREATED) 
                        
                else: 
                    return Response({
                        "status": False,
                        "code" : status.HTTP_200_OK,
                        "message" : "Cannot create project",
                        "data": serializer.errors,
                        }, status=status.HTTP_200_OK)
            except:
                return Response({
                    "status": False,
                    "code" : status.HTTP_400_BAD_REQUEST,
                    "message" : "Bad request",
                    "data": {},
                    }, status=status.HTTP_400_BAD_REQUEST)

        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data": {},
                }, status=status.HTTP_401_UNAUTHORIZED)


class ProjectDetailsAPIView(APIView):
    
    """API View for Group feed details"""

    permission_classes = (IsAuthenticated,)

    def get(self, request, pk,  format=None):

        """GET method for retrieving the data"""

        try:
            project = Project.objects.get(pk=pk) 

            if project is not None:                   
                serializer = ProjectSerializer(project, context={"request": request})
                return Response({
                   
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Successfully fetched project details",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
                    
        except Project.DoesNotExist:
            return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "project not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)


class ProjectDeleteAPIView(APIView):

    """API View to delete Project"""

    permission_classes = (IsAuthenticated,)

    def delete(self, request, pk, format=None):

        """DELETE method to delete the data"""

        if  request.user.is_authenticated:
            try:
                project = Project.objects.get(pk=pk)      
                project.delete()
                return Response({
                        "status": True,
                        "code" : status.HTTP_200_OK,
                        "message" : "Successfully deleted project",
                        "data":{}
                        }, status=status.HTTP_200_OK)

            except Project.DoesNotExist:
                return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Project not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)
        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{}
                }, status=status.HTTP_401_UNAUTHORIZED)


class ProjectUpdateAPIView(APIView):
    """API View to update existing Project"""

    permission_classes = (IsAuthenticated,)
    serializer_class = ProjectSerializer

    def put(self, request, pk, format=None):

        """PUT method to update the data"""

        if  request.user.is_authenticated:
           
            project = Project.objects.filter(pk=pk).exists()  
            if not project:
                return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "Project not found",
                    "data":{}
                    }, status=status.HTTP_404_NOT_FOUND)

        
            project = Project.objects.get(pk=pk) 
            serializer = ProjectSerializer(project, data=request.data,partial=True, context={"request": request})
            
            if serializer.is_valid():
                serializer.save()
                return Response({
                    
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Successfully updated project",
                    "data": serializer.data,
                    }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "status": False,
                    "code" : status.HTTP_200_OK,
                    "message" : "project serializer validation error",
                    "data": serializer.errors,
                    }, status=status.HTTP_200_OK)      
    
        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data":{},
                }, status=status.HTTP_401_UNAUTHORIZED)                

