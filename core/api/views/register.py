from rest_framework.response import Response
from rest_framework import status
from core.api.serializers import  RegisterUserSerializer, OtpSerializer
from core.models import User
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from core.utils.twilio import send_otp
from rest_auth.utils import jwt_encode
from django.utils import timezone
from datetime import datetime, timedelta, date
from core.api.response import MyAPIResponse
from django.db.models import Q


# API FOR User

class RegisterUserApiView(APIView):
    serializer_class = RegisterUserSerializer
    permission_classes = (AllowAny,)

    def post(self, request, format=None):

        data= request.data
        username = data['name']
        mobile_num = "{0}{1}".format(data['country_code'],data['mobile'])
        data._mutable = True
        request.data['mobile'] = mobile_num
        request.data['sent_time'] = datetime.now()
        request.data['username'] = username

        check_mobile_num = User.objects.filter(Q(mobile=mobile_num) |Q(username=username)).exists()
        
        if check_mobile_num:
            mobile_obj = User.objects.get(mobile=mobile_num)
            otp =send_otp(mobile_obj.name, mobile_obj.mobile)
            mobile_obj.code = otp
            mobile_obj.sent_time = datetime.now()
            mobile_obj.save()
            serializer = RegisterUserSerializer(mobile_obj)

            return Response({
                "status": True,
                "code" : status.HTTP_200_OK,
                "message" : "Otp has been sent on your mobile number",
                "data":serializer.data
                }, status=status.HTTP_200_OK)
        
        otp=send_otp(username, mobile_num)
        request.data['code'] = otp
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid(raise_exception = True):

            serializer.save()
            return Response({
                "status": True,
                "code" : status.HTTP_200_OK,
                "message" : "Otp has been sent on your mobile number",
                "data": serializer.data,
            }, status=status.HTTP_200_OK) 
                
        else: 
            return Response({

                "status": False,
                "code" : status.HTTP_400_BAD_REQUEST,
                "message" : "",
                "data": serializer.errors,
                }, status=status.HTTP_400_BAD_REQUEST)
       
       
class UserOtpVerificationAPIView(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        data= request.data
        data._mutable = True
        mobile = request.data['mobile']
        user = User.objects.get(mobile=mobile)
        sent_time = user.sent_time + timedelta(seconds=560)
        sent_time = sent_time.astimezone(timezone.utc).replace(tzinfo=None)

        if datetime.now() > sent_time:

            return Response({
                "status": False,
                "code" : status.HTTP_400_BAD_REQUEST,
                "message" : "OTP is expired",
                "data":{}
                }, status=status.HTTP_400_BAD_REQUEST)

                    
        if request.data['code'] == '' or 'code' not in request.data:
            
            return Response({
                "status": False,
                "code" : status.HTTP_400_BAD_REQUEST,
                "message" : "OTP is required",
                "data":{}
                }, status=status.HTTP_400_BAD_REQUEST)

        else:
            if int(request.data['code']) == int(user.code):
                user.otp_verified = True
                user.save()
                serializer=OtpSerializer(user)
                token = jwt_encode(user)
            
                serializer.data['api_token'] = token
                newdict={'api_token':token}
                newdict.update(serializer.data)

                return Response({
                    "status": True,
                    "code" : status.HTTP_200_OK,
                    "message" : "Registration completed successfully",
                    "data":newdict,
                    }, status=status.HTTP_200_OK)
        
            else:
                return Response({
                    "status": False,
                    "code" : status.HTTP_404_NOT_FOUND,
                    "message" : "OTP is not match.",
                    "data":{}

                    }, status=status.HTTP_404_NOT_FOUND)
   


class ResendCodeApi(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        data= request.data

        mobile = request.data['mobile']
        check_mobile_num = User.objects.filter(mobile=mobile).exists()

        if not check_mobile_num:
            return Response({
                "status": False,
                "code" : status.HTTP_404_NOT_FOUND,
                "message" : "Mobile number has not registered yet"
                }, status=status.HTTP_400_BAD_REQUEST)
        

        mobile_obj = User.objects.get(mobile=mobile)

        try:
            otp=send_otp(mobile_obj.name, mobile_obj.mobile)
            mobile_obj.code = otp
            mobile_obj.sent_time = datetime.now()
            mobile_obj.save()

            return Response({
                "status": True,
                "code" : status.HTTP_200_OK,
                "message" : "Code has been sent on your mobile",
                "data":{},
                }, status=status.HTTP_200_OK)

        except:  
            return Response({
                "status": False,
                "code" : status.HTTP_400_BAD_REQUEST,
                "message" : "Mobile number has not registered yet",
                "data": {}

                }, status=status.HTTP_400_BAD_REQUEST)

                     
          