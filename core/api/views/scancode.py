from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework.permissions import IsAuthenticated, AllowAny

from core.models.activity import Activity
from core.models.activity_user import ActivityUser

# .................................................................................
# Scan code API
# .................................................................................


class ScanCodeAPI(APIView):

    """API View for Scan code """

    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):

        if  request.user.is_authenticated:
            mobile = request.user.mobile
            qr_code = request.data['qr_code']

            try:
                check_activity = Activity.objects.filter(id=qr_code).exists()

                if not check_activity:
                    return Response({
                        "status": False,
                        "code" : status.HTTP_400_BAD_REQUEST,
                        "message" : "Activity is not exist",
                        "data": {},
                    }, status=status.HTTP_400_BAD_REQUEST)


                check_activity = Activity.objects.get(id=qr_code)

                if check_activity.status=='privte' and check_activity.project.status=='active' :
                    check_user = ActivityUser.objects.filter(activity=check_activity,mobile=mobile).exists()
                
                    if check_user:

                        return Response({
                        "status": True,
                        "code" : status.HTTP_200_OK,
                        "message" : "Scan Successfully",
                        "data": {"id":check_activity.id},
                    }, status=status.HTTP_200_OK)

                    else:
                        return Response({
                        "status": False,
                        "code" : status.HTTP_400_BAD_REQUEST,
                        "message" : "You do not have permission to view",
                        "data": {},
                    }, status=status.HTTP_400_BAD_REQUEST)


                elif check_activity.status=='public' and check_activity.project.status=='active':
                        return Response({
                            "status": True,
                            "code" : status.HTTP_200_OK,
                            "message" : "Scan Successfully",
                            "data": {"id":check_activity.id},
                        }, status=status.HTTP_200_OK)

                else:
                    return Response({
                        "status": False,
                        "code" : status.HTTP_400_BAD_REQUEST,
                        "message" : "You do not have permission to view",
                        "data": {},
                    }, status=status.HTTP_400_BAD_REQUEST)   

            except:
                return Response({

                    "status": False,
                    "code" : status.HTTP_400_BAD_REQUEST,
                    "message" : "You do not have permission to view",
                    "data": {},
                }, status=status.HTTP_400_BAD_REQUEST)

        else: 
            return Response({
                "status": False,
                "code" : status.HTTP_401_UNAUTHORIZED,
                "message" : "Unauthorised User",
                "data": {},
                }, status=status.HTTP_401_UNAUTHORIZED)

