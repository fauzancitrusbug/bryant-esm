# Generated by Django 3.0.5 on 2020-08-10 04:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20200708_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='discription',
            field=models.TextField(default=1, verbose_name='Image Discription'),
            preserve_default=False,
        ),
    ]
