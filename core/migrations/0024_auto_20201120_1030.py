# Generated by Django 3.0.5 on 2020-11-20 05:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0023_auto_20201119_1617'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activity',
            name='qr_tag',
        ),
        migrations.RemoveField(
            model_name='user',
            name='has_admin_access',
        ),
        migrations.AddField(
            model_name='activity',
            name='qr_code_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='QR_name'),
        ),
        migrations.DeleteModel(
            name='QrTag',
        ),
    ]
