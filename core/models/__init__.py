from .users import User


from django.conf import settings

from rest_framework.authtoken.models import Token as DefaultTokenModel

from core.utils import import_callable

# Register your models here.

TokenModel = import_callable(
    getattr(settings, 'REST_AUTH_TOKEN_MODEL', DefaultTokenModel))



from .activity import Activity
from .activity_user import ActivityUser
from .project import Project
from .media_file_models import Photo, Video, Document
from imagekit.models import ImageSpecField
from .categories import PhotoCategory, VideoCategory, DocumentCategory