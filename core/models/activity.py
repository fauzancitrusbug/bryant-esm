from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _
from django.db import models

# ------------------------------------------------------------------------
# Activity  Model
# ------------------------------------------------------------------------


class Activity(models.Model):

    """This model stores the data into Activity table in db"""

    STATUS= (
        ("public", _("PUBLIC")),
        ("privte", _("PRIVATE")),   
    )

    title = CharField(_("Title"), blank=True, null=True,  max_length=255)
    status = CharField(_("Status"), blank=True, null=True,  max_length=255,choices=STATUS)
    discription = models.TextField()
    project = models.ForeignKey("core.project", on_delete=models.CASCADE, related_name="project_actvity", blank=True, null=True) 
    qr_code = models.ImageField(upload_to="qr_code",null=True,blank=True,help_text=_("QR code"),verbose_name=_("QR code"))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    tag_name = CharField(_("Tag_name"),blank=True, null=True, max_length=255)


    class Meta:
        verbose_name="Activity"   
        verbose_name_plural = "Activities" 

    def __str__(self):
        return "{0}".format(self.title)    
 