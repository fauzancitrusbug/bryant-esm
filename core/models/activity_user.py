from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _
from django.db import models

# ------------------------------------------------------------------------
# ActivityUser  Model
# ------------------------------------------------------------------------


class ActivityUser(models.Model):

    """This model stores the data into ActivityUser table in db"""

    name = CharField(_("Name"), blank=True, null=True,  max_length=255)
    mobile = CharField(_("Mobile"), blank=True, null=True,  max_length=255)
    activity = models.ForeignKey("core.activity", on_delete=models.CASCADE, related_name="activity_activity_user") 
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    

    class Meta:
        verbose_name="Activity User"   
        verbose_name_plural = "Activity Users" 

    def __str__(self):
        return "{0}".format(self.name)    
 