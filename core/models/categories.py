from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _
from django.db import models

# ------------------------------------------------------------------------
# Categoru  Model
# ------------------------------------------------------------------------


class PhotoCategory(models.Model):

    """This model stores the data into PhotoCategory table in db"""

    name = CharField(_("Title"), blank=True, null=True,  max_length=255)
    activity = models.ForeignKey("core.activity",blank=True,null=True,on_delete=models.CASCADE)
    def __str__(self):
        return "{0}".format(self.name)

class VideoCategory(models.Model):

    """This model stores the data into VideoCategory table in db"""

    name = CharField(_("Title"), blank=True, null=True,  max_length=255)
    activity = models.ForeignKey("core.activity",blank=True,null=True,on_delete=models.CASCADE)

    def __str__(self):
        return "{0}".format(self.name)

class DocumentCategory(models.Model):

    """This model stores the data into DocumentCategory table in db"""

    name = CharField(_("Title"), blank=True, null=True,  max_length=255)
    activity = models.ForeignKey("core.activity",blank=True,null=True,on_delete=models.CASCADE)

    def __str__(self):
        return "{0}".format(self.name)                
 