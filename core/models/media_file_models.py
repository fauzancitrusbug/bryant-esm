from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _
from django.db import models
from core.utils.image_thumbnail import make_thumbnail
from django.db.models.signals import  post_save
from django.dispatch.dispatcher import receiver

# ------------------------------------------------------------------------
# Photo, Video, Document  Model
# ------------------------------------------------------------------------


class Photo(models.Model):

    """This model stores the data into Photo table in db"""
    
    name = CharField(_("Name"), blank=True, null=True,  max_length=255)
    url = models.FileField(upload_to='photo')
    activity = models.ForeignKey("core.activity", on_delete=models.CASCADE, related_name="activity_photo") 
    date = models.DateTimeField(auto_now_add=True,null=True)
    thumb = models.FileField(upload_to='thumb', blank=True, null=True)
    main = models.FileField(upload_to='main', blank=True, null=True)
    discription = models.TextField(verbose_name="Image Discription")
    photo_category = models.ForeignKey("core.photocategory",on_delete=models.CASCADE,blank=True, null=True)

    class Meta:
        verbose_name="Photo"   
        verbose_name_plural = "Photos" 

    def save(self, *args, **kwargs):
        super(Photo, self).save(*args, **kwargs)



class Video(models.Model):

    """This model stores the data into Video table in db"""

    name = CharField(_("Name"), blank=True, null=True,  max_length=255)
    url = CharField(_("URL"), blank=True, null=True,  max_length=255)
    activity = models.ForeignKey("core.activity", on_delete=models.CASCADE, related_name="activity_video") 
    date = models.DateTimeField(auto_now_add=True, null=True)
    thumb = models.ImageField(upload_to="video/thumb",null=True,blank=True,help_text=_("Video Thumb"),verbose_name=_("Video Thumb"))
    main = models.ImageField(upload_to="video/main",null=True,blank=True,help_text=_("Video Main"),verbose_name=_("Video Main"))
    uploaded = models.BooleanField(default=False) 
    video_category = models.ForeignKey("core.videocategory",on_delete=models.CASCADE,blank=True, null=True)

    class Meta:
        verbose_name="Video"   
        verbose_name_plural = "Videos"  
    
    def save(self, *args, **kwargs):
        super(Video, self).save(*args, **kwargs)


class Document(models.Model):

    """This model stores the data into Document table in db"""

    name = CharField(_("Name"), blank=True, null=True,  max_length=255)
    url = models.FileField(upload_to='document')
    activity = models.ForeignKey("core.activity", on_delete=models.CASCADE, related_name="activity_document") 
    date = models.DateTimeField(auto_now_add=True, null=True)
    thumb = models.FileField(upload_to='thumb', blank=True, null=True)
    main = models.FileField(upload_to='main', blank=True, null=True)
    document_category = models.ForeignKey("core.documentcategory",on_delete=models.CASCADE,blank=True, null=True)

    class Meta:
        verbose_name="Document"   
        verbose_name_plural = "Documents" 

    def save(self, *args, **kwargs):
        super(Document, self).save(*args, **kwargs)       


@receiver(post_save, sender=Photo)
def create_thumbnail(sender, instance, created, **kwargs):
    if instance.url:
        make_thumbnail(instance.thumb, instance.url, (200, 200), 'thumb')
        make_thumbnail(instance.main, instance.url, (100, 100), 'main')
        print("success")
        return