from django.db.models import CharField
from django.utils.translation import ugettext_lazy as _
from django.db import models
from.activity import Activity

# ------------------------------------------------------------------------
# Activity  Model
# ------------------------------------------------------------------------


class Project(models.Model):

    """This model stores the data into Activity table in db"""

    STATUS= (
        ("active", _("Active")),
        ("inactive", _("Inactive")),   
    )

    title = CharField(_("Title"), blank=True, null=True,  max_length=255)
    status = CharField(_("Status"), blank=True, null=True,  max_length=255,choices=STATUS)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name="Project"   
        verbose_name_plural = "Projects" 

    def __str__(self):
        return "{0}".format(self.title)    

    
    def get_activity_count(self):
        total_activity=Activity.objects.filter(project=self.id).count()   
        return total_activity
 