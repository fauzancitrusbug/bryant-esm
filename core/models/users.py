from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from datetime import datetime
import uuid
# ------------------------------------------------------------------------
# User Model
# ------------------------------------------------------------------------


class User(AbstractUser):

    """This model stores the data into User table in db"""

    # First Name and Last Name do not cover name patterns
    # around the globe.

    name = CharField(_("Name"), blank=True, max_length=255)
    avatar = models.FileField(upload_to='avatar', default='sample.jpg', max_length=255)
    mobile = CharField(_("Mobile"), blank=True, max_length=255, unique=True)
    code = CharField(_("Code"), blank=True, max_length=255)
    otp_verified = models.BooleanField(default=False) 
    sent_time = models.DateTimeField(default=timezone.now, null=False, blank=False, verbose_name='link send time')
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True,)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True,)
    project = models.ManyToManyField("core.project",blank=True)
    uuid = models.UUIDField(blank=True, null=True,default=uuid.uuid4)
      
    def get_absolute_url(self):
        return reverse("core:user-detail", kwargs={"username": self.username})
