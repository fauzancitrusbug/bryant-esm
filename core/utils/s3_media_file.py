import boto3
from boto.s3.key import Key
import boto
import requests
from django.core.files import File
from urllib import request as rq
from bryant.settings import (
    AWS_STORAGE_BUCKET_NAME,
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY,
    AWS_BUCKET_URL
)
import os


def get_s3_store_directory_file(directory_path="media/photo/"):
    """
    param:directory_path
    """
    try:
        file_list =[]
        sesssion = boto3.Session(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
        s3 = sesssion.resource("s3")
        bucket_name = AWS_STORAGE_BUCKET_NAME
        bucket = s3.Bucket(bucket_name)
        s3_file_list = bucket.objects.filter(Prefix=directory_path)
        for obj in s3_file_list:
            path, filename = os.path.split(obj.key)
            file_list.append({"filename": filename, "url": f"{AWS_BUCKET_URL}{obj.key}"})
        return file_list
    except:
        return []


def upload_from_url_to_s3(url):
    try:
        c = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
        b = c.get_bucket(AWS_STORAGE_BUCKET_NAME, validate=False)
        r = requests.get(url)
        if r.status_code == 200:
            k = Key(b)
            k.key = "media/photo/image12.png"
            k.content_type = r.headers['content-type']
            resp = k.set_contents_from_string(r.content)
            return resp
    except:
           return "Fail"


def upload_photo_s3_url_to_file(url, photo_object):
    result = rq.urlretrieve(url)
    response = photo_object.url.save(
            os.path.basename(url),
            File(open(result[0], 'rb'))
            )
    return response


def get_filename_from_url(url):
    try:
        filename = url.split("/")[-1]
        return filename
    except:
        return "Test"
