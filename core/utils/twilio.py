from twilio.rest import Client
from bryant.settings import AUTH_TOKEN_TWILIO, ACCOUNT_SID, TWILIO_FORM_NUMBER
import random

  
def send_otp(name, recv):        
    my = recv    
    name = name
    try:
        account_sid =  ACCOUNT_SID
        auth_token  = AUTH_TOKEN_TWILIO
        client = Client(account_sid, auth_token)
        otp = random.randint(1111, 9999)    
        my_msg = "Hi " + name + " !" + "\n" + "Your account verification code is : " + str(otp)
        message = client.messages.create(body=my_msg, from_= TWILIO_FORM_NUMBER, to=my) 
        print(message)

    except Exception as e:
        print(e)

    return otp



